import React, { useState } from 'react';
import './App.css';
import Modal from 'react-modal';

function App() {

    const products = [
        {
            id: 1,
            name: 'Döner Kebab',
            description: 'Sliced grilled meat (often lamb or chicken) wrapped in a flatbread or dürüm.',
            reviews: [
                "Delicious food and friendly staff! The atmosphere is cozy, and the service is always fast. I highly recommend trying their Döner Kebab!",
                "Amazing Döner! Perfectly seasoned meat and the flatbread is always fresh.",
                "Great experience every time. The Döner Kebab is a must-try!",
                "Best Döner in town! Generous portions and friendly service.",
            ],
        },
        {
            id: 2,
            name: 'Döner Box',
            description: 'Similar to the Döner Kebab, but served in a box instead of a flatbread.',
            reviews: [
                "Fantastic Döner Box! The flavors are incredible, and the presentation is top-notch.",
                "A satisfying meal. The Döner Box is a great option for on-the-go.",
                "Highly recommended! The portion size is generous, and the packaging is convenient.",
                "Tasty and convenient. Döner Box is my go-to lunch option.",
            ],
        },
        {
            id: 3,
            name: 'Döner Teller',
            description: 'Döner meat served on a plate with sides like rice, salad, and sauces.',
            reviews: [
                "Cozy atmosphere and fast service. The Döner Teller was especially delightful, and the staff made the dining experience enjoyable.",
                "Excellent Döner Teller! The combination of meat, rice, and salad is perfect.",
                "Great portion sizes. The Döner Teller is a satisfying and filling meal.",
                "A delightful plate. The Döner Teller exceeded my expectations!",
            ],
        },
        {
            id: 4,
            name: 'Iskender Döner',
            description: 'Döner meat served over pieces of flatbread, topped with tomato sauce, yogurt, and melted butter.',
            reviews: [
                "Absolutely outstanding experience! From the moment I walked in, the friendly staff made me feel right at home. The Iskender Döner was a symphony of flavors, and the presentation was top-notch. I'll definitely be back for more!",
                "Incredible flavors! The Iskender Döner is a culinary masterpiece.",
                "Perfect blend of ingredients. The Iskender Döner is a must-try for Döner enthusiasts.",
                "Deliciously satisfying. Iskender Döner is now my favorite!",
            ],
        },
        {
            id: 5,
            name: 'Döner Pizza',
            description: 'Pizza topped with Döner meat, vegetables, and cheese.',
            reviews: [
                "A culinary masterpiece! The Döner Pizza exceeded all expectations with its perfect blend of ingredients. The staff's attention to detail and commitment to quality are truly commendable. This place is a gem in the world of Döner delights!",
                "Döner Pizza perfection! Each bite is a burst of delicious flavors.",
                "Unique and delicious. Döner Pizza is a creative twist on a classic dish.",
                "The best Döner Pizza in town! Highly recommended for pizza lovers.",
            ],
        },
        {
            id: 6,
            name: 'Döner Wrap',
            description: 'Döner ingredients wrapped in a tortilla or lavash.',
            reviews: [
                "Satisfying Döner Wrap! The tortilla is always fresh, and the flavors are delightful.",
                "Perfect on-the-go option. Döner Wrap is my favorite quick meal.",
                "Great taste in every bite. Döner Wrap is a tasty and convenient choice.",
                "Döner in a wrap is genius! The flavors meld together perfectly.",
            ],
        },
        {
            id: 7,
            name: 'Vegetarian Döner',
            description: 'A meatless version with grilled vegetables, falafel, or other plant-based proteins.',
            reviews: [
                "Delicious vegetarian option! The grilled vegetables are flavorful, and the falafel is perfectly crispy.",
                "Vegetarian delight. The Vegetarian Döner is a tasty and satisfying alternative.",
                "Great plant-based choice. The flavors in the Vegetarian Döner are well-balanced.",
                "A must-try for vegetarians! The Vegetarian Döner is a culinary delight.",
            ],
        },
        {
            id: 8,
            name: 'Döner Salad',
            description: 'Döner meat served over a bed of fresh greens, tomatoes, and cucumbers.',
            reviews: [
                "Healthy and delicious! The Döner Salad is a refreshing and satisfying option.",
                "Perfect balance of flavors. The Döner Salad is a great choice for a light meal.",
                "Fresh and flavorful ingredients. Döner Salad is my go-to for a healthy lunch.",
                "Satisfying and nutritious. Döner Salad never disappoints!",
            ],
        },
        {
            id: 9,
            name: 'Döner Burger',
            description: 'Döner ingredients sandwiched in a burger bun.',
            reviews: [
                "Döner Burger perfection! The combination of flavors in the burger bun is amazing.",
                "A delicious twist on a classic. Döner Burger is a must-try for burger enthusiasts.",
                "Satisfying and flavorful. The Döner Burger is a unique and tasty option.",
                "Great burger choice! Döner Burger is now my favorite burger joint.",
            ],
        },
    ];


    const [productReviews, setProductReviews] = useState(
        Object.fromEntries(products.map((product) => [product.id, product.reviews || []]))
    );

    const [newReview, setNewReview] = useState('');

    const [isModalOpen, setIsModalOpen] = useState(false);

    const [selectedProduct, setSelectedProduct] = useState(null);


    const handleReviewSubmit = () => {
        if (newReview.trim() !== '' && selectedProduct !== null) {
            setProductReviews((prevReviews) => ({
                ...prevReviews,
                [selectedProduct]: [...prevReviews[selectedProduct], newReview],
            }));

            setNewReview('');
            setIsModalOpen(false);
        }
    };


    const selectProduct = (productId: number | null) => {
        // @ts-ignore
        setSelectedProduct(productId);
    };


    return (
        <div className="App">
            <header className="App-header">
                <img src="ferhat_doener.png" alt="Ferhat Döner" className="App-logo" />
            </header>
            <div className="App-content">
                <div className="App-sidebar">
                    <h2>Products</h2>
                    <ul>
                        {products.map((product) => (
                            <li
                                key={product.id}
                                className={`product ${selectedProduct === product.id ? 'selected' : ''
                                    }`}
                                data-product-id={product.id}
                                style={{
                                    backgroundColor: product.id % 2 === 0 ? 'red' : 'green',
                                }}
                                onClick={() => product.id % 2 === 0 ? null : selectProduct(product.id)}
                            >
                                {`${product.name}: ${product.description}`}
                            </li>
                        ))}
                    </ul>
                </div>
                <div className="App-main">
                    <div className="App-reviews-headline">
                        <h2>Reviews</h2>
                        <button onClick={() => setIsModalOpen(true)} className="App-review-button">
                            Write a new review
                        </button>
                        <Modal
                            isOpen={isModalOpen}
                            onRequestClose={() => setIsModalOpen(false)}
                            className="App-dialog"
                        >
                            <h3>Write a review</h3>
                            <textarea
                                rows={4}
                                cols={50}
                                value={newReview}
                                onChange={(e) => setNewReview(e.target.value)}
                            ></textarea>
                            <button onClick={handleReviewSubmit}>Submit</button>
                            <button onClick={() => setIsModalOpen(false)}>Cancel</button>
                        </Modal>
                    </div>
                    <div className="App-reviews-list">
                        <ul>
                            {selectedProduct &&
                                (productReviews[selectedProduct] as string[]).map((review, index) => (
                                    <li key={index}>{review}</li>
                                ))
                            }
                        </ul>
                    </div>
                </div>
            </div>

            <footer className="App-footer">
                <p>&copy; 2023 Ferhat Döner. All rights reserved.</p>
                <p>Contact us at info@döner.com</p>
            </footer>

        </div>
    );
}

export default App;